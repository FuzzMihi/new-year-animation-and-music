from PIL import Image
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
import pygame
import sys
import os

pygame.init()
pygame.mixer.init()
os.getcwd()

class Player(QWidget):
    def __init__(self):
        super().__init__()
        uic.loadUi("Assets/Player.ui",self)
        self.started = False
        self.gif = QMovie("Assets/NewYear.gif")
        self.label.setText("")
        self.label.setMovie(self.gif)
        self.startStop.clicked.connect(lambda:self.startStopFunc())
    
    def startStopFunc(self):
        if not self.started:
            pygame.mixer.music.load("Assets/AndjeoskaHimna.wav")
            self.label.setMovie(self.gif)
            self.started = True
            pygame.mixer.music.play()
            self.gif.start()
            self.startStop.setText("Stop")
        
        else:
            self.started = False
            self.gif.stop()
            self.label.setMovie(None)
            pygame.mixer.music.pause()
            self.startStop.setText("Start")

app = QApplication(sys.argv)
player = Player()
player.show()
sys.exit(app.exec())